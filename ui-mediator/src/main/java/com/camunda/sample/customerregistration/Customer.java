package com.camunda.sample.customerregistration;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Customer implements Serializable {
  
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue
  private long id;
  
  private String firstName;
  private String lastName;
  private String address;
  private long zipCode;
  private String city;
  
  private String companyDetails;
  
  private Long scoringPoints;
  
  public long getId() {
    return id;
  }
  
  public void setId(long id) {
    this.id = id;
  }
  
  public String getFirstName() {
    return firstName;
  }
  
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  
  public String getLastName() {
    return lastName;
  }
  
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  
  public String getAddress() {
    return address;
  }
  
  public void setAddress(String address) {
    this.address = address;
  }
  
  public long getZipCode() {
    return zipCode;
  }
  
  public void setZipCode(long zipCode) {
    this.zipCode = zipCode;
  }
  
  public String getCity() {
    return city;
  }
  
  public void setCity(String city) {
    this.city = city;
  }

  
  public String getCompanyDetails() {
    return companyDetails;
  }

  
  public void setCompanyDetails(String companyDetails) {
    this.companyDetails = companyDetails;
  }

  
  public Long getScoringPoints() {
    return scoringPoints;
  }

  
  public void setScoringPoints(Long scoringPoints) {
    this.scoringPoints = scoringPoints;
  }

  @Override
  public String toString() {
    return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + ", zipCode=" + zipCode + ", city=" + city
            + ", companyDetails=" + companyDetails + ", scoringPoints=" + scoringPoints + "]";
  }

}
