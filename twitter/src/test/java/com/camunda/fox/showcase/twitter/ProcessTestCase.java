package com.camunda.fox.showcase.twitter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.test.TestHelper;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.ActivitiTestCase;
import org.activiti.engine.test.Deployment;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;

public class ProcessTestCase  {
  
  @Rule
  public ActivitiRule activitiRule = new ActivitiRule();;

  public void testDeployment() {
  }
	
  @Test
  @Deployment(resources = "TwitterDemoProcess.bpmn")  
  public void testRejectedPath() {
    Map<String, Object> variables = new HashMap<String, Object>();
    variables.put("content", "We will never see this content on Twitter");
    variables.put("email", "bernd.ruecker@camunda.com");

    System.out.println("### RuntimeService");
    ProcessInstance processInstance = activitiRule.getRuntimeService().startProcessInstanceByKey("TwitterDemoProcess", variables);
    System.out.println("### RuntimeService fertig");
    String id = processInstance.getId();
    System.out.println("Started process instance id " + id);
        
    Assert.assertThat(activitiRule.getRuntimeService().getActiveActivityIds(id), JUnitMatchers.hasItem("Review_Tweet_3"));
    
    // PFad durch den Prozess anschauen
    List<HistoricActivityInstance> historyActivities = activitiRule.getProcessEngine().getHistoryService().createHistoricActivityInstanceQuery()
      .processInstanceId(processInstance.getId())
      .finished()
      .orderByActivityId().asc()
      .list();

    assertEquals(2, historyActivities.size());
    assertEquals("new_tweet_3", historyActivities.get(0).getActivityId());
    assertEquals("Service Task", historyActivities.get(1).getActivityName());

    
    

    Task task = activitiRule.getTaskService().createTaskQuery().taskAssignee("kermit").singleResult();
    variables.put("approved", Boolean.FALSE);
    variables.put("comments", "No, we will not publish this on Twitter");
    variables.put("binary", new HashMap<String, Serializable>());
    activitiRule.getTaskService().complete(task.getId(), variables);
    
    TestHelper.assertProcessEnded(activitiRule.getProcessEngine(), id);

    HistoricProcessInstance historicProcessInstance = activitiRule.getHistoryService().createHistoricProcessInstanceQuery().processInstanceId(id).singleResult();
    assertNotNull(historicProcessInstance);

    System.out.println("Finished, took " + historicProcessInstance.getDurationInMillis() + " millis");
  }
  
}